package br.com.rzandonai.buscatweepsweb.json.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Url_info {

    @SerializedName("expanded_url")
    @Expose
    private Object expandedUrl;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("indices")
    @Expose
    private List<Integer> indices = null;

    public Object getExpandedUrl() {
        return expandedUrl;
    }

    public void setExpandedUrl(Object expandedUrl) {
        this.expandedUrl = expandedUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Integer> getIndices() {
        return indices;
    }

    public void setIndices(List<Integer> indices) {
        this.indices = indices;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(indices).append(expandedUrl).append(url).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Url_info) == false) {
            return false;
        }
        Url_info rhs = ((Url_info) other);
        return new EqualsBuilder().append(indices, rhs.indices).append(expandedUrl, rhs.expandedUrl).append(url, rhs.url).isEquals();
    }

}
