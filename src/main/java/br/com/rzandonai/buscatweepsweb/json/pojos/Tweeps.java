package br.com.rzandonai.buscatweepsweb.json.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Tweeps {

    @SerializedName("statuses")
    @Expose
    private List<Status> statuses = null;

    public List<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(statuses).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Tweeps) == false) {
            return false;
        }
        Tweeps rhs = ((Tweeps) other);
        return new EqualsBuilder().append(statuses, rhs.statuses).isEquals();
    }

}
