package br.com.rzandonai.buscatweepsweb.json.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class Url {

    @SerializedName("urls")
    @Expose
    private List<Url_info> urls = null;

    public List<Url_info> getUrls() {
        return urls;
    }

    public void setUrls(List<Url_info> urls) {
        this.urls = urls;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(urls).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Url) == false) {
            return false;
        }
        Url rhs = ((Url) other);
        return new EqualsBuilder().append(urls, rhs.urls).isEquals();
    }

}
