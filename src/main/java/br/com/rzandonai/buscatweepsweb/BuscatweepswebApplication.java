package br.com.rzandonai.buscatweepsweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuscatweepswebApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuscatweepswebApplication.class, args);
	}
}
