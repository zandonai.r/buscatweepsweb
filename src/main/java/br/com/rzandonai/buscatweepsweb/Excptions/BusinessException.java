package br.com.rzandonai.buscatweepsweb.Excptions;

public class BusinessException extends Exception {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -8169618008693298492L;
	public BusinessException() { }
	  public BusinessException(String message) {
	    super(message);
	  }
}
