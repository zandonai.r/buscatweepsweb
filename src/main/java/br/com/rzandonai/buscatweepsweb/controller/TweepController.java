package br.com.rzandonai.buscatweepsweb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rzandonai.buscatweepsweb.Excptions.BusinessException;
import br.com.rzandonai.buscatweepsweb.dtos.MostMentionsDTO;
import br.com.rzandonai.buscatweepsweb.dtos.MostRelevantsDTO;
import br.com.rzandonai.buscatweepsweb.service.ProcessadorTweepSerivce;

@RestController
public class TweepController {
	@Autowired
	ProcessadorTweepSerivce service;
	
	@RequestMapping("/most_relevants")
	public ResponseEntity<?> listMostRelevants() throws BusinessException {
		List<MostRelevantsDTO> retorno = service.getMostRelevantsTweeps();
		if(retorno == null || retorno.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(retorno,HttpStatus.OK);
	}
	
	@RequestMapping("/most_mentions")
	public ResponseEntity<?> listMostMentions() throws BusinessException {
		MostMentionsDTO retorno = service.getMostMentionsTweeps();
		if(retorno == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(retorno,HttpStatus.OK);
	}
}
