package br.com.rzandonai.buscatweepsweb.dtos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.rzandonai.buscatweepsweb.json.pojos.Status;
import br.com.rzandonai.buscatweepsweb.json.pojos.Url;
import br.com.rzandonai.buscatweepsweb.json.pojos.Url_info;

public class MostRelevantsDTO implements Comparable<MostRelevantsDTO> {

	public MostRelevantsDTO(Status status) {
		if (status == null) {
			return;
		}
		createdAt = status.getCreatedAt();
		retweetCount = status.getRetweetCount();
		text = status.getText();
		favoriteCount = status.getFavoriteCount();

		if (status.getUser() != null) {
			followersCount = status.getUser().getFollowersCount();
			screenName = status.getUser().getScreenName();
			profileLink = status.getUser().getUrl();
		}

		if (status.getEntities() != null) {
			if (status.getEntities().getUrls() != null && !status.getEntities().getUrls().isEmpty()) {
				for (Url url : status.getEntities().getUrls()) {
					if (url.getUrls() != null && !url.getUrls().isEmpty()) {
						for (Url_info urlInfo : url.getUrls()) {
							if (urlInfo.getUrl() != null && !urlInfo.getUrl().trim().isEmpty()) {
								link = urlInfo.getUrl();
								break;
							}
						}
					}
					if (link != null) {
						break;
					}

				}
			}
		}
	}

	@SerializedName("followers_count")
	@Expose
	private Integer followersCount;
	@SerializedName("screen_name")
	@Expose
	private String screenName;
	@SerializedName("profile_link")
	@Expose
	private String profileLink;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("link")
	@Expose
	private String link;
	@SerializedName("retweet_count")
	@Expose
	private Integer retweetCount;
	@SerializedName("text")
	@Expose
	private String text;
	@SerializedName("favorite_count")
	@Expose
	private Integer favoriteCount;

	public Integer getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(Integer followersCount) {
		this.followersCount = followersCount;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getProfileLink() {
		return profileLink;
	}

	public void setProfileLink(String profileLink) {
		this.profileLink = profileLink;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	/*
	 * O recurso `most_mentions` deve retornar os tweets agregados por usu�rio,
	 * aplicando os mesmos crit�rios de ordena��o dos mais relevantes.
	 */

	@Override
	public int compareTo(MostRelevantsDTO o) {
		int retorno = this.followersCount.compareTo(o.getFollowersCount());
		if (retorno == 0) {
			retorno = this.retweetCount.compareTo(o.getRetweetCount());
			if (retorno == 0) {
				retorno = this.favoriteCount.compareTo(o.getFavoriteCount());
			}
		}
		return retorno;
	}
}
