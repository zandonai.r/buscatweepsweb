package br.com.rzandonai.buscatweepsweb.dtos;

import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.rzandonai.buscatweepsweb.json.pojos.Status;
import br.com.rzandonai.buscatweepsweb.json.pojos.Url;
import br.com.rzandonai.buscatweepsweb.json.pojos.Url_info;

public class PessoaDTO {
	@SerializedName("screen_name")
	@Expose
	private String screenName;
	@SerializedName("profile_link")
	@Expose
	private String profileLink;
	@SerializedName("created_at")
	@Expose
	private String createdAt;
	@SerializedName("favorite_count")
	@Expose
	private Integer favoriteCount;
	@SerializedName("followers_count")
	@Expose
	private Integer followersCount;
	@SerializedName("link")
	@Expose
	private String link;
	@SerializedName("text")
	@Expose
	private String text;
	@SerializedName("retweet_count")
	@Expose
	private Integer retweetCount;

	public PessoaDTO(List<Status> value) {
		// TODO Auto-generated constructor stub
		Status status = value.get(0);
		if (status == null) {
			return;
		}
		createdAt = status.getCreatedAt();
		retweetCount = status.getRetweetCount();
		text = status.getText();
		favoriteCount = status.getFavoriteCount();

		if (status.getUser() != null) {
			followersCount = status.getUser().getFollowersCount();
			screenName = status.getUser().getScreenName();
			profileLink = status.getUser().getUrl();
		}

		if (status.getEntities() != null) {
			if (status.getEntities().getUrls() != null && !status.getEntities().getUrls().isEmpty()) {
				for (Url url : status.getEntities().getUrls()) {
					if (url.getUrls() != null && !url.getUrls().isEmpty()) {
						for (Url_info urlInfo : url.getUrls()) {
							if (urlInfo.getUrl() != null && !urlInfo.getUrl().trim().isEmpty()) {
								link = urlInfo.getUrl();
								break;
							}
						}
					}
					if (link != null) {
						break;
					}

				}
			}
		}
		
		
		
		
		
		/*
		
		
		  .followersCount(status.getUser().getFollowersCount())
          .screenName(status.getUser().getScreenName())
          .createdAt(status.getCreatedAt())
          .retweetCount(status.getRetweetCount())
          .text(status.getText())
          .favoriteCount(status.getFavoriteCount());

  CollectionUtils.emptyIfNull(status.getUser().getEntities().getUrl().getUrls())
          .stream()
          .findFirst()
          .ifPresent(urlItem -> {
              builder.link(urlItem.getExpandedUrl());
              builder.profileLink(urlItem.getUrl());
          });
		*/
		
		
		
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getProfileLink() {
		return profileLink;
	}

	public void setProfileLink(String profileLink) {
		this.profileLink = profileLink;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public Integer getFollowersCount() {
		return followersCount;
	}

	public void setFollowersCount(Integer followersCount) {
		this.followersCount = followersCount;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(text).append(favoriteCount).append(createdAt).append(link)
				.append(retweetCount).append(screenName).append(followersCount).append(profileLink).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof PessoaDTO) == false) {
			return false;
		}
		PessoaDTO rhs = ((PessoaDTO) other);
		return new EqualsBuilder().append(text, rhs.text).append(favoriteCount, rhs.favoriteCount)
				.append(createdAt, rhs.createdAt).append(link, rhs.link).append(retweetCount, rhs.retweetCount)
				.append(screenName, rhs.screenName).append(followersCount, rhs.followersCount)
				.append(profileLink, rhs.profileLink).isEquals();
	}

}
