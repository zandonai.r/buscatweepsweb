package br.com.rzandonai.buscatweepsweb.dtos;

import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.google.gson.annotations.Expose;
public class MostMentionsDTO {

	@Expose
	Map<String,PessoaDTO> pessoa = null;
	
    public Map<String, PessoaDTO> getPessoa() {
		return pessoa;
	}

	public void setPessoa(Map<String, PessoaDTO> pessoa) {
		this.pessoa = pessoa;
	}

	@Override
    public int hashCode() {
        return new HashCodeBuilder().append(pessoa).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MostMentionsDTO) == false) {
            return false;
        }
        MostMentionsDTO rhs = ((MostMentionsDTO) other);
        return new EqualsBuilder().append(pessoa, rhs.pessoa).isEquals();
    }


}
