package br.com.rzandonai.buscatweepsweb.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.rzandonai.buscatweepsweb.json.pojos.Tweeps;

@Service
public class BuscaInformacaoService {
	@Value("${usersample}")
	private String usersample;

	@Value("${tweepurl}")
	private String server;

	private RestTemplate rest;
	private HttpHeaders headers;
	private HttpStatus status;

	public void init() {
		this.rest = new RestTemplate();
		this.headers = new HttpHeaders();
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "*/*");
		String user = usersample;
		if (System.getenv("HTTP_USERNAME") != null && !System.getenv("HTTP_USERNAME").isEmpty()) {
			user = System.getenv("HTTP_USERNAME");
		}
		headers.add("Username", user);
	}

	public Tweeps get() {
		init();
		HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
		ResponseEntity<Tweeps> responseEntity = rest.exchange(server, HttpMethod.GET, requestEntity, Tweeps.class);
		this.setStatus(responseEntity.getStatusCode());
		return responseEntity.getBody();
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
