package br.com.rzandonai.buscatweepsweb.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rzandonai.buscatweepsweb.Excptions.BusinessException;
import br.com.rzandonai.buscatweepsweb.dtos.MostMentionsDTO;
import br.com.rzandonai.buscatweepsweb.dtos.MostRelevantsDTO;
import br.com.rzandonai.buscatweepsweb.dtos.PessoaDTO;
import br.com.rzandonai.buscatweepsweb.json.pojos.Status;
import br.com.rzandonai.buscatweepsweb.json.pojos.Tweeps;
import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * 
 * @author ricardozandonai Classe que procesa os tweeps
 */
@Service
public class ProcessadorTweepSerivce {

	@Autowired
	private BuscaInformacaoService service;

	/**
	 * Busca todos os tweeps com a condi��o
	 * 
	 * tweet que mencione o usu�rio da Locaweb tweet que n�o � reply para tweets da
	 * Locaweb
	 * 
	 * @return
	 * @throws BusinessException
	 */
	private List<Status> buscaTweepsComCondicao() throws BusinessException {
		Tweeps tweeps = service.get();
		if (tweeps.getStatuses() != null && tweeps.getStatuses().isEmpty()) {
			throw new BusinessException("N�o existem tweeps para a busca");
		}
		// tweet que mencione o usu�rio da Locaweb
		return tweeps.getStatuses().stream().filter(s -> s.getText().contains("@locaweb "))
				// tweet que n�o � reply para tweets da Locaweb
				.filter(s -> new Integer(42).equals(s.getInReplyToUserId())).collect(toList());
	}

	public List<MostRelevantsDTO> getMostRelevantsTweeps() throws BusinessException {
		return buscaTweepsComCondicao().stream().map(s -> new MostRelevantsDTO(s)).sorted().collect(toList());

	}

	public MostMentionsDTO getMostMentionsTweeps() throws BusinessException {

		MostMentionsDTO mostMnetions = new MostMentionsDTO();
		/*
		*/
		Map<String, List<Status>> mapa = buscaTweepsComCondicao().stream()
				.collect(Collectors.groupingBy(a -> a.getUser().getName()));

		// converter os objetos e reordena
		mostMnetions.setPessoa(ShortAndConvert(mapa));
		return mostMnetions;

	}

	private static Map<String, PessoaDTO> ShortAndConvert(Map<String, List<Status>> mapa) {

		List<Map.Entry<String, List<Status>>> list = new LinkedList<Map.Entry<String, List<Status>>>(mapa.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, List<Status>>>() {
			public int compare(Map.Entry<String, List<Status>> o1, Map.Entry<String, List<Status>> o2) {
				return Integer.compare(o1.getValue().size(), o2.getValue().size());
			}
		});

		java.util.Map<String, PessoaDTO> sortedMap = new LinkedHashMap<String, PessoaDTO>();
		for (Map.Entry<String, List<Status>> entry : list) {
			sortedMap.put(entry.getKey(), new PessoaDTO(entry.getValue()));
		}
		return sortedMap;
	}
}
