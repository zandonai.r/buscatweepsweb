package br.com.rzandonai.buscatweepsweb;

import static org.junit.Assert.assertThat;

import org.junit.Before;

import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.rzandonai.buscatweepsweb.json.pojos.Tweeps;
import br.com.rzandonai.buscatweepsweb.service.BuscaInformacaoService;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BuscaTweepsTest {

	@Autowired
	BuscaInformacaoService buscaInformacaoService;
	private Tweeps get = null;
	
	@Before
	public void buscaTweepsTest() {
		get = buscaInformacaoService.get();
	}
	
	@Test
	public void verificaSeVazioTest() {
		assertThat(get, is(notNullValue()));
	}

	@Test
	public void verificaParseTest() {
		assertThat(get.getStatuses(), is(notNullValue()));
		assertThat(get.getStatuses().isEmpty(), is(false));
	}
	
}
