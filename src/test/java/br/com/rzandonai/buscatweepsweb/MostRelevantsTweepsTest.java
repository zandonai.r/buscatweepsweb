package br.com.rzandonai.buscatweepsweb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import br.com.rzandonai.buscatweepsweb.controller.TweepController;

import java.nio.charset.Charset;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TweepController.class)
public class MostRelevantsTweepsTest{
	   @Autowired
	   private MockMvc mvc;

	   
	   private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
	            MediaType.APPLICATION_JSON.getSubtype(),
	            Charset.forName("utf8"));
	   
	   @MockBean
	   private TweepController tweepController;

	   @Test
	   public void getArrivals() throws Exception {
		   MvcResult mvcReturn = mvc.perform(get("/most_relevants").contentType(contentType)).andDo(print())
           .andExpect(status().isOk()).andReturn();
		   System.out.println(mvcReturn.getResponse().getContentAsString());
	     
	   }

}
